"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    brevet_start = arrow.get(brevet_start_time)

    if control_dist_km > brevet_dist_km:
      control_dist_km = brevet_dist_km

    TOTAL1 = 200/34 # used > 200
    TOTAL2 = 200/32 + TOTAL1 # used > 400
    TOTAL3 = 200/30 + TOTAL2 # used > 600
    TOTAL4 = 400/28 + TOTAL3 # used > 1000
    timedelta = 0
    if control_dist_km >= 1000:
      # 5th bracket
      timedelta = (control_dist_km-1000)/26+TOTAL4
    elif control_dist_km >= 600:
      # 4th bracket
      timedelta = (control_dist_km-600)/28+TOTAL3
    elif control_dist_km >= 400:
      # 3rd bracket
      timedelta = (control_dist_km-400)/30+TOTAL2
    elif control_dist_km >= 200:
      # 2nd bracket
      timedelta = (control_dist_km-200)/32+TOTAL1
    else:
      # 1st bracket
      timedelta = (control_dist_km)/34
    td_hours = int(timedelta)
    td_minutes = round((timedelta-td_hours)*60)
    open_time = brevet_start.shift(hours=+td_hours,minutes=+td_minutes).isoformat()
    return open_time


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    brevet_start = arrow.get(brevet_start_time)

    if control_dist_km > brevet_dist_km:
      control_dist_km = brevet_dist_km

    TOTAL1 = 200/15 # used > 200
    TOTAL2 = 200/15 + TOTAL1 # used > 400
    TOTAL3 = 200/15 + TOTAL2 # used > 600
    TOTAL4 = 400/11.428 + TOTAL3 # used > 1000
    timedelta = 0
    if control_dist_km >= 1000:
      # 5th bracket
      timedelta = (control_dist_km-1000)/13.333+TOTAL4
    elif control_dist_km >= 600:
      # 4th bracket
      timedelta = (control_dist_km-600)/11.428+TOTAL3
    elif control_dist_km >= 400:
      # 3rd bracket
      timedelta = (control_dist_km-400)/15+TOTAL2
    elif control_dist_km >= 200:
      # 2nd bracket
      timedelta = (control_dist_km-200)/15+TOTAL1
    else:
      # 1st bracket
      timedelta = (control_dist_km)/15

    # Check if higher than overall time for a given brevet
    # 13:30 for 200 KM, 20:00 for 300 KM, 27:00 for 400 KM, 40:00 for 600 KM, and 75:00 for 1000 KM
    isover = control_dist_km == brevet_dist_km
    if isover:
      if brevet_dist_km == 200:
          timedelta = 13.5
      elif brevet_dist_km == 300:
          timedelta = 20
      elif brevet_dist_km == 400:
          timedelta = 27
      elif brevet_dist_km == 600:
          timedelta = 40
      elif brevet_dist_km == 1000:
        timedelta = 75

    td_hours = int(timedelta)
    td_minutes = round((timedelta-td_hours)*60)
    if control_dist_km <= 0:
      close_time = brevet_start.shift(hours=+1).isoformat()
    else:
      close_time = brevet_start.shift(hours=+td_hours,minutes=+td_minutes).isoformat()
    return close_time