<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <h1>List of laptops</h1>
        <ul>
            <?php
            echo "---------------- listAll<br>";
            $json = file_get_contents('http://web:5000/listAll');
            $obj = json_decode($json);
	        $opens = $obj->open;
            echo "open times";
            foreach ($opens as $l) {
                echo "<li>$l</li>";
            }
            echo "close times";
            $closes = $obj->close;
            foreach ($closes as $l) {
                echo "<li>$l</li>";
            }
            echo "----------------<br>";

            echo "---------------- listOpenOnly<br>";
            $json = file_get_contents('http://web:5000/listOpenOnly');
            $obj = json_decode($json);
            $opens = $obj->open;
            echo "open times";
            foreach ($opens as $l) {
                echo "<li>$l</li>";
            }
            echo "----------------<br>";

            echo "---------------- listCloseOnly<br>";
            $json = file_get_contents('http://web:5000/listCloseOnly');
            $obj = json_decode($json);
            echo "close times";
            $closes = $obj->close;
            foreach ($closes as $l) {
                echo "<li>$l</li>";
            }
            echo "----------------<br>";

            echo "---------------- listAll/csv<br>";
            $csv = file_get_contents('http://web:5000/listAll/csv');
            echo $csv;
            echo "<br>----------------<br>";

            echo "---------------- listOpenOnly/csv<br>";
            $csv = file_get_contents('http://web:5000/listOpenOnly/csv');
            echo $csv;
            echo "<br>----------------<br>";

            echo "---------------- listCloseOnly/csv<br>";
            $csv = file_get_contents('http://web:5000/listCloseOnly/csv');
            echo $csv;
            echo "<br>----------------<br>";

            echo "<br>---------------- /listOpenOnly/json?top=2<br>";
            $json = file_get_contents('http://web:5000/listOpenOnly/json?top=2');
            $obj = json_decode($json);
            $opens = $obj->open;
            echo "open times";
            foreach ($opens as $l) {
                echo "<li>$l</li>";
            }
            echo "----------------<br>";
            ?>
        </ul>
    </body>
</html>
