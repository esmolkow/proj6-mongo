import acp_times

import nose
import logging
import arrow
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

UTC_START = "2017-01-01T00:00:00+00:00"
# Using spec examples from https://rusa.org/pages/acp-brevet-control-times-calculator

def format_diff(isotime):
	diff = arrow.get(isotime)-arrow.get(UTC_START)
	days = diff.days
	hours,remainder = divmod(diff.seconds,3600)
	minutes,seconds = divmod(remainder,60)
	ret = str(hours+(days*24))+"H"+"{:02d}".format(minutes)
	print(ret)
	return ret

def test_times():
	# Assuming starting time for race is 2017-01-01 UTC
	# Example 1
	brevet = 200
	assert format_diff(acp_times.open_time(60,  brevet, UTC_START)) == "1H46"
	assert format_diff(acp_times.open_time(120, brevet, UTC_START)) == "3H32"
	assert format_diff(acp_times.open_time(175, brevet, UTC_START)) == "5H09"
	assert format_diff(acp_times.open_time(205, brevet, UTC_START)) == "5H53"

	assert format_diff(acp_times.close_time(60, brevet, UTC_START)) == "4H00"
	assert format_diff(acp_times.close_time(120, brevet, UTC_START)) == "8H00"
	assert format_diff(acp_times.close_time(175, brevet, UTC_START)) == "11H40"
	assert format_diff(acp_times.close_time(205, brevet, UTC_START)) == "13H30"

	#Example 2
	brevet = 600
	assert format_diff(acp_times.open_time(100, brevet, UTC_START)) == "2H56"
	assert format_diff(acp_times.open_time(200, brevet, UTC_START)) == "5H53"
	assert format_diff(acp_times.open_time(350, brevet, UTC_START)) == "10H34"
	assert format_diff(acp_times.open_time(550, brevet, UTC_START)) == "17H08"

	assert format_diff(acp_times.close_time(550, brevet, UTC_START)) == "36H40"
	assert format_diff(acp_times.close_time(605, brevet, UTC_START)) == "40H00"

	#Example 2
	brevet = 1000
	assert format_diff(acp_times.open_time(890,  brevet, UTC_START)) == "29H09"
	assert format_diff(acp_times.close_time(890, brevet, UTC_START)) == "65H23"

def test_oddities():
	brevet = 200
	assert format_diff(acp_times.close_time(0,  brevet, UTC_START)) == "1H00"
	assert format_diff(acp_times.close_time(10, brevet, UTC_START)) == "0H40"