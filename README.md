# Project 6: Brevet time calculator with Ajax and REST interface

Reimplement the RUSA ACP controle time calculator with flask and ajax and mongodb.

Also contains several REST endpoints to retrive stored data along with a sample client to utilize the endpoints.

Credits to Michal Young for the initial version of this code.

Modifications done by Elyse Smolkowski, esmolkow@uoregon.edu

## Endpoints

* Standard JSON responses
    * "http://<host:port>/listAll" should return all open and close times in the database
    * "http://<host:port>/listOpenOnly" should return open times only
    * "http://<host:port>/listCloseOnly" should return close times only

* Specifying CSV or JSON
    * "http://<host:port>/listAll/csv" should return all open and close times in CSV format
    * "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format

    * "http://<host:port>/listAll/json" should return all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" should return close times only in JSON format

* Specifying how many results to retrieve

    * "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format


## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). The description is ambiguous, but the examples help. Part of finishing this project is clarifying anything that is not clear about the requirements, and documenting it clearly.  

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.  


### Usage ###

* Clone the repo.

* Run `docker-compose build` from the `brevets` directory.

* Next, run `docker-compose up`.

* The calculator will be running on localhost:5000, while the client will be running on localhost:5001

* Enter some values into the calculator and click submit.

* View the results on the client at localhost:5001

## Interpretation of ACP

The rules are followed exactly as stateed on https://rusa.org/pages/acp-brevet-control-times-calculator and https://rusa.org/pages/rulesForRiders.

Specifically Article 9 which sets max times for given bravets is of interest and required modifications to the base code. It is assumed that all checkpoints are located within the same time zone.

Another detail that required additional attention was the timezone of the client vs the server. Time calculations are all entered in, and performed in UTC if that matters.